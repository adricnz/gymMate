package com.example.adric.gymmate;

import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Tab3Stats extends Fragment {
    Button btnViewAll, btnRefresh, btnCalories, btnDistance, btnLength;
    DatabaseHelper myDB;
    int exerciseCount, totalCalories, totalDistance, totalLength, bestCalories, bestLength, bestDistance, currentCalories, currentDistance, currentLength;
    TextView textViewAverageLength, textViewAverageDistance, textViewAverageCalories, textViewBestCalories, textViewBestLength, textViewBestDistance;
    LineChart lineChart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab3stats, container, false);

        myDB = new DatabaseHelper(getActivity());
        textViewAverageLength = (TextView) rootView.findViewById(R.id.text_avg_length);
        textViewAverageCalories = (TextView) rootView.findViewById(R.id.text_avg_calories);
        textViewAverageDistance = (TextView) rootView.findViewById(R.id.text_avg_distance);
        textViewBestCalories = (TextView) rootView.findViewById(R.id.text_best_calories);
        textViewBestDistance = (TextView) rootView.findViewById(R.id.text_best_distance);
        textViewBestLength = (TextView) rootView.findViewById(R.id.text_best_length);

        btnRefresh = (Button) rootView.findViewById(R.id.btn_refresh);
        btnCalories = (Button) rootView.findViewById(R.id.btn_calories);
        btnLength = (Button) rootView.findViewById(R.id.btn_length);
        btnDistance = (Button) rootView.findViewById(R.id.btn_distance);

        lineChart = (LineChart) rootView.findViewById(R.id.lineChart);


        calculations("null"); //metric as in calories/distance
        refresh(); //refresh button
        graphButtons(); // graph selection buttons
        return rootView;
    }
    //refreshes statistics
    public void refresh() {
        btnRefresh.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        calculations("null");
                    }
                }
        );

    }

    public void graphButtons() {
        btnCalories.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        calculations("calories");
                    }
                }
        );

        btnDistance.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        calculations("distance");
                    }
                }
        );
        btnLength.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        calculations("length");
                    }
                }
        );
    }


    //shows all exercises as popup
    public void viewAll() {
        btnViewAll.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Cursor res = myDB.getAllData();
                        StringBuffer buffer = new StringBuffer();
                        while (res.moveToNext()) {
                            buffer.append("ID: " + res.getString(0) + "\n");
                            buffer.append("Date: " + res.getString(1) + "\n");
                            buffer.append("Length: " + res.getString(2) + "\n");
                            buffer.append("Calories: " + res.getString(3) + "\n");
                            buffer.append("Distance:" + res.getString(4) + "\n\n");
                            buffer.append("-------\n");
                        }
                        showMessage("Data", buffer.toString());
                    }
                }
        );
    }

    //calculates average calories, distance, and length of exercises and sets TextViews and graph
    public void calculations(String metric) {
        exerciseCount = totalCalories = totalDistance = totalLength = bestLength = bestCalories = bestDistance = 0;
        Cursor res = myDB.getAllData();
        ArrayList<Entry> yAXEScal = new ArrayList<>();
        ArrayList<Entry> yAXESdist = new ArrayList<>();
        ArrayList<Entry> yAXESLength = new ArrayList<>();
        if (res.getCount() == 0) {
            return;
        }

        while (res.moveToNext()) {
            exerciseCount++;
            currentLength = Integer.parseInt(res.getString(2));
            currentCalories = Integer.parseInt(res.getString(3));
            currentDistance = Integer.parseInt(res.getString(4));

            //checking for bests
            if (currentLength > bestLength) {
                bestLength = currentLength;
            }
            if (currentDistance > bestDistance) {
                bestDistance = currentDistance;
            }
            if (currentCalories > bestCalories) {
                bestCalories = currentCalories;
            }


            yAXEScal.add(new Entry(exerciseCount, currentCalories));
            yAXESdist.add(new Entry(exerciseCount, currentDistance));
            yAXESLength.add(new Entry(exerciseCount, currentLength));


            totalCalories += currentCalories;
            totalLength += currentLength;
            totalDistance += currentDistance;


        }

        textViewAverageLength.setText(Integer.toString(totalLength / exerciseCount) + " min");
        textViewAverageCalories.setText(Integer.toString(totalCalories / exerciseCount));
        textViewAverageDistance.setText(Integer.toString(totalDistance / exerciseCount) + " km");
        textViewBestLength.setText(Integer.toString(bestLength) + " min");
        textViewBestCalories.setText(Integer.toString(bestCalories));
        textViewBestDistance.setText(Integer.toString(bestDistance) + " km");

        //graph setup
        ArrayList<ILineDataSet> lineDataSets = new ArrayList<>();

        LineDataSet lineDataSet1 = new LineDataSet(yAXEScal, "calories");
        LineDataSet lineDataSet2 = new LineDataSet(yAXESdist, "distance");
        LineDataSet lineDataSet3 = new LineDataSet(yAXESLength, "length");

        //graph formatting
        lineDataSet2.setColor(Color.RED);
        lineDataSet3.setColor(Color.GREEN);
        lineChart.getLegend().setTextColor(Color.WHITE);
        lineChart.getAxisLeft().setTextColor(Color.WHITE);
        lineChart.getAxisRight().setTextColor(Color.WHITE);
        lineChart.getXAxis().setTextColor(Color.WHITE);
        Description des = lineChart.getDescription();
        des.setEnabled(false);

        //selects graph subject, displays all by default
        switch (metric) {
            case "calories":
                lineDataSets.add(lineDataSet1);
                break;
            case "distance":
                lineDataSets.add(lineDataSet2);
                break;
            case "length":
                lineDataSets.add(lineDataSet3);
                break;
            default:
                lineDataSets.add(lineDataSet1);
                lineDataSets.add(lineDataSet2);
                lineDataSets.add(lineDataSet3);
        }
        lineChart.setData(new LineData(lineDataSets));

    }


    //displays popup message
    public void showMessage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }
}
