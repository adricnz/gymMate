package com.example.adric.gymmate;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;


public class MainActivity extends AppCompatActivity implements
        Tab1Menu.MenuSwapTabListener, Tab2Input.InputSwapTabListener, Tab4AddType.AddSwapTabListener {

    @Override
    public void swapPage(int i) {
        mViewPager.setCurrentItem(i);
    }
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
       TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);

        }
        //switch tab based on position
        @Override
        public Fragment getItem(int position) {

            switch(position){

                case 0:
                    Tab1Menu tab1 = new Tab1Menu();
                    return tab1;
                case 1:
                    Tab2Input tab2 = new Tab2Input();
                    return tab2;
                case 2:
                    Tab3Stats tab3 = new Tab3Stats();
                    return tab3;
                case 3:
                    Tab4AddType tab4 = new Tab4AddType();
                    return tab4;
                default:
                    return null;
            }
        }



        @Override
        public int getCount() {
            return 4;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch(position){
                case 0:
                    return "MENU";
                case 1:
                    return "INPUT";
                case 2:
                    return "STATS";
                case 3:
                    return "ADD TYPE";
            }
            return null;
        }
    }
}
