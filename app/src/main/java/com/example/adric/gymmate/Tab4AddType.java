package com.example.adric.gymmate;

import android.content.Context;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Tab4AddType extends Fragment {


    Button btnAddType, btnClear;
    EditText editType;
    AddSwapTabListener swapTabListener;
    DatabaseHelper myDB;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.tab4addtype, container, false);
        btnAddType = (Button) rootView.findViewById(R.id.btn_add_type);
        editType = (EditText) rootView.findViewById(R.id.edit_text_add_type);
        btnClear = (Button) rootView.findViewById(R.id.btn_clear);
        myDB = new DatabaseHelper(getActivity());

        btnAddType.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isInserted = myDB.insertNewType(
                                editType.getText().toString()
                        );
                        if (isInserted) {
                            Toast.makeText(getActivity(), "New Type Inserted", Toast.LENGTH_LONG).show();
                            swapTabListener.swapPage(1);
                        } else {
                            Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        );

        clearDB();

        return rootView;
    }
    //clears exercise DB completely
    public void clearDB() {
        btnClear.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDB.clearDatabase();
                    }
                }
        );
    }

    //interface to allow swapping of tabs with buttons
    public interface AddSwapTabListener {
        public void swapPage(int i);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            swapTabListener = (AddSwapTabListener) getActivity();
        } catch (Exception e) {
        }
    }

}





