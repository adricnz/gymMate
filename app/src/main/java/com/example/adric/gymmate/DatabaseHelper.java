package com.example.adric.gymmate;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "exercises.db";
    public static final String TABLE_NAME = "exercise_table";
    public static final String COL_1 = "ex_id";
    public static final String COL_2 = "datetime";
    public static final String COL_3 = "length";
    public static final String COL_4 = "calories";
    public static final String COL_5 = "distance";
    public static final String COL_6 = "type";
    public static final String TABLE_NAME_TWO = "type_table";
    public static final String COL_1_TWO = "type_id";
    public static final String COL_2_TWO = "type_name";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " (" + COL_1 + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COL_2 + " TEXT, "
                + COL_3 + " TEXT, "
                + COL_4 + " TEXT, "
                + COL_5 + " TEXT, "
                + COL_6 + " TEXT);");

        db.execSQL("CREATE TABLE " + TABLE_NAME_TWO + " (" + COL_1_TWO + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COL_2_TWO + " TEXT);");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_TWO);
        onCreate(db);
    }

    public boolean insertData(String datetime, String length, String calories, String distance, String type) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_2, datetime);
        contentValues.put(COL_3, length);
        contentValues.put(COL_4, calories);
        contentValues.put(COL_5, distance);
        contentValues.put(COL_6, type);

        long result = db.insert(TABLE_NAME, null, contentValues); //identifies if successful

        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean insertNewType(String type) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COL_2_TWO, type);

        long result = db.insert(TABLE_NAME_TWO, null, cv);

        if (result == -1) {
            return false;

        } else {
            return true;
        }

    }

    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("Select * FROM " + TABLE_NAME + ";", null);
        return res;
    }

    public Cursor getTypes() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_NAME_TWO + ";", null);
        return res;
    }

    public void clearDatabase() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("delete FROM " + TABLE_NAME + ";");
        db.execSQL("delete FROM " + TABLE_NAME_TWO + ";");
        //default exercises
        insertNewType("Cycling");
        insertNewType("Rowing");
        insertNewType("Running");

    }


}
