package com.example.adric.gymmate;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;


public class Tab2Input extends Fragment {

    DatabaseHelper myDB;
    EditText editLength, editDistance, editCalories;
    Button btnSubmit, btnViewAll, btnNewType, btnStats;
    Calendar calendar;
    Spinner spinner;
    ArrayList<String> ls;
    InputSwapTabListener swapTabListener;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab2input, container, false);


        myDB = new DatabaseHelper(getActivity());
        //EditTexts
        editLength = (EditText) rootView.findViewById(R.id.edit_length);
        editCalories = (EditText) rootView.findViewById(R.id.edit_calories);
        editDistance = (EditText) rootView.findViewById(R.id.edit_distance);
        //Buttons
        btnSubmit = (Button) rootView.findViewById(R.id.btn_submit);
        btnViewAll = (Button) rootView.findViewById(R.id.btn_viewall);
        btnNewType = (Button) rootView.findViewById(R.id.btn_new_type);
        btnStats = (Button) rootView.findViewById(R.id.btn_stats);

        //calendar
        calendar = timeSetup();

        spinner = (Spinner) rootView.findViewById(R.id.spinner);
        ls = new ArrayList<>();

        //starts methods
        addData();
        viewAll();
        getTypes();
        setSpinner();
        swapToAddType();
        swapToStats();
        return rootView;
    }

    //sets exercise selection spinner
    public void setSpinner() {
        ls = getTypes();
        ArrayAdapter<String> adapter = new
                ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, ls);

        spinner.setAdapter(adapter);
        spinner.setSelection(spinner.getCount() - 1);

    }
    //retrieves types of exercise from db
    public ArrayList<String> getTypes() {
        Cursor res = myDB.getTypes();
        ArrayList<String> ls = new ArrayList<String>();

        while (res.moveToNext()) {
            ls.add(res.getString(1));
        }
        return ls;

    }
    //swaps to addType fragment
    public void swapToAddType() {
        btnNewType.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        swapTabListener.swapPage(3);
                    }
                }

        );
    }

    //swaps to stats fragment
    public void swapToStats() {
        btnStats.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        swapTabListener.swapPage(2);
                    }
                }
        );
    }


    //returns set up calendar
    public Calendar timeSetup() {
        String[] ids = TimeZone.getAvailableIDs(12 * 60 * 60 * 1000);
        System.out.println("Current Time");

        SimpleTimeZone nzt = new SimpleTimeZone(12 * 60 * 60 * 1000, ids[0]);
        //DST rules
        nzt.setStartRule(Calendar.OCTOBER, 1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
        nzt.setEndRule(Calendar.APRIL, -1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);

        Calendar calendar = new GregorianCalendar(nzt);
        calendar.setTime(new Date());

        return calendar;
    }

    //returns current date in 28/01/2018 format
    public String getDate() {
        String year, month, dayOfMonth;
        year = Integer.toString(calendar.get(Calendar.YEAR));
        month = Integer.toString(calendar.get(Calendar.MONTH));
        dayOfMonth = Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));

        return dayOfMonth + "/" + month + "/" + year;

    }

    //adds entered data to DB
    public void addData() {
        btnSubmit.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isInserted = myDB.insertData(
                                getDate(),
                                editLength.getText().toString(),
                                editCalories.getText().toString(),
                                editDistance.getText().toString(),
                                spinner.getSelectedItem().toString()

                        );
                        //success/failure alerts
                        if (isInserted) {
                            Toast.makeText(getActivity(), "Data inserted", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "Data not inserted", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        );
    }

    //shows a popup with all exercises (debug only)
    public void viewAll() {
        btnViewAll.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Cursor res = myDB.getAllData();
                        if (res.getCount() == 0) {
                            showMessage("Error", "No data found");
                            return;
                        }
                        StringBuffer buffer = new StringBuffer();
                        while (res.moveToNext()) {
                            //buffer.append("ID: " + res.getString(0) + "\n");
                            buffer.append("Date: " + res.getString(1) + "\n");
                            buffer.append("Length: " + res.getString(2) + "\n");
                            buffer.append("Calories: " + res.getString(3) + "\n");
                            buffer.append("Distance: " + res.getString(4) + "\n");
                            buffer.append("Type: " + res.getString(5) + "\n");
                            buffer.append("-------\n");
                        }
                        showMessage("Exercises", buffer.toString());
                    }
                }
        );
    }


    //displays a popup message
    public void showMessage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }


    public interface InputSwapTabListener {
        public void swapPage(int i);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            swapTabListener = (InputSwapTabListener) getActivity();
        } catch (Exception e) {
        }
    }


}
