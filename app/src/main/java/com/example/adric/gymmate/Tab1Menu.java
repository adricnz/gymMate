package com.example.adric.gymmate;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class Tab1Menu extends Fragment {
    Button btnInput, btnStats, btnHelp;
    MenuSwapTabListener swapTabListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.tab1menu, container, false);

        //buttons
        btnInput = (Button) rootView.findViewById(R.id.btn_input);
        btnStats = (Button) rootView.findViewById(R.id.btn_stats);
        btnHelp = (Button) rootView.findViewById(R.id.btn_help);

        //swap tab listeners
        btnInput.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        swapTabListener.swapPage(1);
                    }
                }
        );

        btnStats.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        swapTabListener.swapPage(2);
                    }
                }

        );
        displayHelp();
        return rootView;
    }

    //interface to allow swapping of tabs with buttons
    public interface MenuSwapTabListener {
        public void swapPage(int i);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            swapTabListener = (MenuSwapTabListener) getActivity();
        } catch (Exception e) {
        }
    }
    //displays popup message
    public void showMessage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

    public void displayHelp(){
        btnHelp.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StringBuffer buffer = new StringBuffer();
                        buffer.append("Swipe right and left to select Input and Stats screens.\n" +
                                "Input your exercises and click the three graph buttons to select a graph");
                        showMessage("Help",buffer.toString());

                    }
                }
        );
    }
}





